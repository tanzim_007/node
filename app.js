import express from 'express';

const app = express();
const port = 3000;

// Route for handling GET requests to the root URL
app.get('/', (req, res) => {
  res.send('Hello, this is a simple Express application!');
});

// Start the server
const server = app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
});

export default server;
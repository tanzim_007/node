import { assert } from 'chai';
import  {factorial} from '../factorial.js';


describe('Factorial', function () {

        it('Should return 1 for 0', function () {
            assert.equal(factorial(0), 1);
        });
    
        it('Should return 1 for 1', function () {
            assert.equal(factorial(1), 1);
        });
    
        it('Should return 120 for 5', function () {
            assert.equal(factorial(5), 120);
        });
    
        it('Should return 3628800 for 10', function () {
            assert.equal(factorial(10), 3628800);
        });
    });





